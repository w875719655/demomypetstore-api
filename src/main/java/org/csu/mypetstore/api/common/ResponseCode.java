package org.csu.mypetstore.api.common;

import lombok.Getter;

@Getter
public enum ResponseCode {

    SUCCESS(0,"SUCCESS"),
    ERROR(1,"ERROR"),
    NEED_LOGIN(10,"NEED_LOGIN"),
    ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT");

    private final int code;
    private  final String descripion;

    ResponseCode(int code,String descripion){
        this.code = code;
        this.descripion = descripion;
    }
}
