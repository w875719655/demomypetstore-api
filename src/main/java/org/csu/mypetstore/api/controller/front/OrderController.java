package org.csu.mypetstore.api.controller.front;

import org.csu.mypetstore.api.common.CommonResponse;
import  org.csu.mypetstore.api.entity.Order;
import org.csu.mypetstore.api.Vo.OrderVO;
import org.csu.mypetstore.api.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/order/")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping()
    @ResponseBody
    public CommonResponse<OrderVO> insertOrder(Order order){
        System.out.println(order.getOrderId());
        return orderService.insertOrder(order);
    }

    @GetMapping()
    @ResponseBody
    public CommonResponse<OrderVO> getOrder(@PathVariable("id") String orderId){
        try {
            int id = Integer.valueOf(orderId);
            return orderService.getOrder(id);
        }catch (Exception exception){
            return CommonResponse.createForSuccessMessage("输入的订单号不是数字");
        }
    }

    @GetMapping()
    @ResponseBody
    public CommonResponse<List<Order>> getOrdersByUsername(@PathVariable("id") String username) {
        return orderService.getOrdersByUsername(username);
    }
}