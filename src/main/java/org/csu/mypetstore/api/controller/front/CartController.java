package org.csu.mypetstore.api.controller.front;

import org.csu.mypetstore.api.common.CommonResponse;
import org.csu.mypetstore.api.entity.Cart;
import org.csu.mypetstore.api.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping()
public class CartController {
    @Autowired
    private CartService cartService;

    @GetMapping()
    @ResponseBody
    public CommonResponse<List<Cart>> getCartByUsername(@PathVariable("id") String username) {
        return cartService.getCartByUsername(username);
    }

    @PostMapping()
    @ResponseBody
    public CommonResponse<Cart> insertCart(Cart cart){
        return cartService.insertCart(cart);
    }

    @PostMapping("{id}/{item}")
    @ResponseBody
    public CommonResponse<Cart> insertCart(@PathVariable("id") String username,@PathVariable("item") String itemId){
        return cartService.insertCart(username,itemId);
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public CommonResponse<List<Cart>> deleteCart(@PathVariable("id") String username){
        return cartService.deleteAllCart(username);
    }

    @DeleteMapping("{id}/items")
    @ResponseBody
    public CommonResponse<Cart> deleteOneCart(@PathVariable("id")String username,@RequestParam("itemId")String itemId){
        return cartService.deleteOneCart(username,itemId);
    }


    @PutMapping("{id}")
    @ResponseBody
    public CommonResponse<Cart> updateCart(@PathVariable("id") String username, Cart cart){
        return cartService.updateCart(username,cart);
    }
}