package org.csu.mypetstore.api.service;

import org.csu.mypetstore.api.Vo.AccountVO;
import org.csu.mypetstore.api.common.CommonResponse;

public interface AccountService {
    CommonResponse<AccountVO> getAccount(String username, String password);
    CommonResponse<AccountVO> getAccount(String username);
    CommonResponse<AccountVO> insertAccount(AccountVO accountVo);
    CommonResponse<AccountVO> updateAccount(String username,AccountVO accountVo);

}