package org.csu.mypetstore.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("cart")
public class Cart {
    @TableId(value = "userId", type = IdType.INPUT)
    private String userid;
    @TableField(value = "itemId")
    private String itemid;
    @TableField(value = "productId")
    private String productid;
    @TableField(value = "description")
    private String description;
    @TableField(value = "inStock")
    private String instock;
    @TableField(value = "quantity")
    private Integer quantity;
    @TableField(value = "listPrice")
    private BigDecimal listprice;
    @TableField(value = "totalCost")
    private BigDecimal totalcost;
}
